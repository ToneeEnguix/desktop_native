import { Button, View, Text, SafeAreaView, StatusBar, TextInput, StyleSheet, Pressable } from 'react-native'
import React, { useState } from 'react'

export default function App() {
  const [newTodo, setNewTodo] = useState("")
  const [todos, setTodos ]= useState([])
  const [updateIndex, setUpdateIndex] = useState(null)
  const [updateText, setUpdateText] = useState("")

  const addTodo = () => {
    if (!newTodo || todos.some(t => t.title === newTodo)) return false
    setTodos  ([ ...todos, { title: newTodo, completed: false } ] )
    setNewTodo("")
  }

  const deleteTodo = (i) => {
    const copy = [...todos]
    copy.splice(i, 1)
    setTodos(copy)
  }

  const updateTodo = (i) => {
    const copy = [...todos]
    todos[i].title = updateText
    setTodos(copy)
    setUpdateText("")
    setUpdateIndex(null)
  }

  return (
    <SafeAreaView style={styles.mainWrapper} >
      <StatusBar/>
      <View style={styles.inputWrapper}>
        <TextInput value={newTodo} style={styles.input} onChangeText={setNewTodo} />
        <Pressable onPress={addTodo} >
          <Text style={styles.addText}>Add</Text>
        </Pressable>
      </View>
      <View style={styles.todosContainer}>
        <Text style={styles.title}>Todos:</Text>
        {todos.map( (todo, i) => {
          return (
            <View key={i} style={styles.fullTodoWrapper}>
              <View style={styles.todoWrapper}>
                <Text style={styles.todo}>{todo.title}</Text>
                <Pressable onPress={() => deleteTodo(i)}>
                  <Text style={styles.bin}>🗑</Text>
                </Pressable>
                <Pressable onPress={() => setUpdateIndex(updateIndex === i ? null : i)}>
                  <Text style={styles.update}>Update</Text>
                </Pressable>
              </View>
              {updateIndex === i && (
                <View style={styles.updateWrapper}>
                  <TextInput style={styles.input} onChangeText={setUpdateText} />
                  <Button onPress={() => updateTodo(i)} title="Submit" />
                </View>
              )}
            </View>
          )
        })}
      </View>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  mainWrapper: {
    flex: 1,
    alignItems: "center",
    marginTop: 100
  },
  inputWrapper: {
    // flexDirection: "row",
    alignItems: "center",
    marginBottom: 50,
    backgroundColor: "#EEE",
    paddingTop: 40,
    paddingRight: 80,
    paddingBottom: 40,
    paddingLeft: 80,
    borderRadius: 40
  },
  input: {
    height: 40,
    fontSize: 25,
    width: "20%",
    minWidth: 200,
    borderColor: "black",
    borderWidth: 1,
    marginBottom: 20
  },
  addText: {
    fontSize: 30
  },
  todosContainer: {

  },
  title: {
    fontSize: 40,
    marginBottom: 20,
    textAlign: "center"
  },
  fullTodoWrapper: {
    backgroundColor: "#DEED",
    marginBottom: 20,
    paddingTop: 10,
    paddingRight: 40,
    paddingBottom: 10,
    paddingLeft: 40,
    borderRadius: 20,
    marginBottom: 20,
  },
  todoWrapper: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  todo: {
    fontSize: 30,
    marginRight: 10,
  },
  bin: {
    marginBottom: -5
  },
  update: {
    color: "gray",
    marginBottom: -5,
    marginLeft: 20
  },
  updateWrapper: {
    marginTop: 20,
  }
})